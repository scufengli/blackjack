const oldName = "lucky1";
const newName = "hiRoller";
const fullName = "Luke Lachman";
const birthDate = "2022-07-04";
const theRiches = 4720;
const glomOnDate = (string) => string + new Date().getTime();

describe("unit 7 day 2 mongoose homework", () => {
  beforeEach(() => {
    cy.visit("/table.html");
  });
  it("should have an api that supports a user's creating a new user when all is POSTed in order, and GETting that user", () => {
    const thisOldName = glomOnDate(oldName);
    const thisFullName = glomOnDate(fullName);
    cy.request("POST", "/api/user/", {
      username: thisOldName,
      fullName: thisFullName,
      password: "inappropriateToKeepInTheClear",
      birthDate: birthDate,
      bankroll: theRiches,
    }).then((response) => {
      expect(response.status).to.eql(200);
      cy.request(`/api/user/${response.body.id}`).then((res2) => {
        expect(res2.body.username).to.eql(thisOldName);
        expect(res2.body.fullName).to.eql(thisFullName);
        expect(res2.body.birthDate.split("T")[0]).to.eql(birthDate);
        expect(res2.body.bankroll).to.eql(theRiches);
        expect(res2.body).not.to.haveOwnProperty("password");
      });
    });
  });

  it("should have an api that prevents a user's creating a new user when username isn't POSTed", () => {
    cy.wait(1000);
    const thisFullName = glomOnDate(fullName);
    cy.request({
      method: "POST",
      url: "/api/user/",
      body: {
        fullName: thisFullName,
        password: "inappropriateToKeepInTheClear",
        birthDate: birthDate,
        bankroll: theRiches,
      },
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eql(400);
    });
  });
  it("should have an api that prevents a user's creating a new user when fullName isn't POSTed", () => {
    cy.wait(1000);
    const thisOldName = glomOnDate(oldName);
    cy.request({
      method: "POST",
      url: "/api/user/",
      body: {
        username: thisOldName,
        password: "inappropriateToKeepInTheClear",
        birthDate: birthDate,
        bankroll: theRiches,
      },
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eql(400);
    });
  });
  it("should have an api that prevents a user's creating a new user when password isn't POSTed", () => {
    cy.wait(1000);
    const thisOldName = glomOnDate(oldName);
    const thisFullName = glomOnDate(fullName);
    cy.request({
      method: "POST",
      url: "/api/user/",
      body: {
        username: thisOldName,
        fullName: thisFullName,
        birthDate: birthDate,
        bankroll: theRiches,
      },
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eql(400);
    });
  });
  it("should have an api that prevents a user's creating a new user when birthDate isn't POSTed", () => {
    cy.wait(1000);
    const thisOldName = glomOnDate(oldName);
    const thisFullName = glomOnDate(fullName);
    cy.request({
      method: "POST",
      url: "/api/user/",
      body: {
        username: thisOldName,
        fullName: thisFullName,
        password: "inappropriateToKeepInTheClear",
        bankroll: theRiches,
      },
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eql(400);
    });
  });
  it("should have an api that prevents a user's creating a new user when bankroll isn't POSTed", () => {
    cy.wait(1000);
    const thisOldName = glomOnDate(oldName);
    const thisFullName = glomOnDate(fullName);
    cy.request({
      method: "POST",
      url: "/api/user/",
      body: {
        username: thisOldName,
        fullName: thisFullName,
        password: "inappropriateToKeepInTheClear",
        birthDate: birthDate,
      },
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eql(400);
    });
  });
  it("should have an api that prevents duplicate users", () => {
    cy.wait(1000);
    const thisOldName = glomOnDate(oldName);
    const thisFullName = glomOnDate(fullName);
    cy.request("POST", "/api/user/", {
      username: thisOldName,
      fullName: thisFullName,
      password: "inappropriateToKeepInTheClear",
      birthDate: birthDate,
      bankroll: 4720,
    }).then((res1) => {
      cy.request({
        method: "POST",
        url: "/api/user/",
        bodu: {
          username: thisOldName,
          fullName: thisFullName,
          password: "inappropriateToKeepInTheClear",
          birthDate: birthDate,
          bankroll: 4720,
        },
        failOnStatusCode: false,
      }).then((res2) => {
        expect(res2.status).to.eql(400);
      });
    });
  });
});
