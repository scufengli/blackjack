const oldName = "lucky1";
const newName = "hiRoller";
const fullName = "Luke Lachman";
const birthDate = "2022-07-04";

describe("unit 7 day 1 vanilla mongo homework", () => {
  beforeEach(() => {
    cy.visit("/table.html");
  });
  it("should have an api that supports a user's creation, access, updation, and deletion", () => {
    cy.request("POST", "/api/user/", {
      username: oldName,
      fullName: fullName,
      password: "inappropriateToKeepInTheClear",
      birthDate: birthDate,
    }).then((createdResponse) => {
      const createdId = createdResponse.body.insertedId;
      cy.request("GET", `/api/user/${createdId}`).then((readResponse) => {
        expect(readResponse.body).not.have.property("password");
        expect(readResponse.body.username).eql(oldName);
        expect(readResponse.body.fullName).eql(fullName);
        expect(readResponse.body.birthDate).eql(birthDate);
        cy.request("PATCH", `/api/user/${createdId}`, {
          username: newName,
        }).then((patchResponse) => {
          expect(patchResponse.body.matchedCount).eql(1);
          cy.request("GET", `/api/user/${createdId}`).then((read2Response) => {
            expect(read2Response.body.username).eql(newName);
            cy.request("DELETE", `/api/user/${createdId}`).then(
              (deleteResponse) => {
                expect(deleteResponse.body.deletedCount).eql(1);
                cy.request("GET", `/api/user/${createdId}`).then(
                  (read3Response) => {
                    // this is the sixth or severalth level of the pyramid of doom ;)
                    expect(read3Response.body).to.be.null;
                  }
                );
              }
            );
          });
        });
      });
    });
  });
});
