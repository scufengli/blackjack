require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require("./api");
const port = process.env.PORT || 3000;
const bodyParser = require("body-parser");

app.use(
  morgan("dev"),
  bodyParser.json({ extended: false }),
  bodyParser.urlencoded({ extended: false })
);
const baseURL = "/2239952"
app.use(baseURL, express.static("public"), express.static("dist"));
app.get(baseURL + "/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});
app.get(baseURL + "/table", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/table.html"));
});
app.use(baseURL + "/api", api);
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
